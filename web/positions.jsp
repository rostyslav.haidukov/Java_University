<%--
  Created by IntelliJ IDEA.
  User: rosty
  Date: 11.11.2018
  Time: 10:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Position</title>
</head>
<body>
<div class="container">
    <table class="table table-striped">
        <c:forEach items="${positions}" var="position">
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Salary</th>
            </tr>
            <tr>
                <td><c:out value="${position.getName()}"/></td>
                <td><c:out value="${position.getDescription()}"/></td>
                <td><c:out value="${position.getSalary()}"/></td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
