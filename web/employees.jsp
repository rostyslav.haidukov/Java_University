<%--
  Created by IntelliJ IDEA.
  User: rosty
  Date: 11.11.2018
  Time: 10:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Employees</title>
</head>
<body>
    <div class="container">
        <table class="table table-striped">
            <c:forEach items="${employees}" var="employee">
                <tr>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Position</th>
                    <th>Salary</th>
                    <th>Department</th>
                    <th>Passport</th>
                    <th>Birth day</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th></th>
                </tr>
                <tr>
                    <td><c:out value="${employee.getFirstName()}"/></td>
                    <td><c:out value="${employee.getLastName()}"/></td>
                    <td><c:out value="${employee.getPosition().getName()}"/></td>
                    <td><c:out value="${employee.getPosition().getSalary()}"/></td>
                    <td><c:out value="${employee.getDepartmentName()}"/></td>
                    <td><c:out value="${employee.getPassport()}"/></td>
                    <td><c:out value="${employee.getBirthDay()}"/></td>
                    <td><c:out value="${employee.getPhone()}"/></td>
                    <td><c:out value="${employee.getEmail()}"/></td>
                    <td>
                        <i id="employee-${employee.getPassport()}-delete"
                           class="fa fa-trash"></i>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</body>
</html>
