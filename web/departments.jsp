<%--
  Created by IntelliJ IDEA.
  User: rosty
  Date: 11.11.2018
  Time: 10:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Department</title>
</head>
<body>
<div class="container">
    <table class="table table-striped">
        <c:forEach items="${departments}" var="department">
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Employee List</th>
            </tr>
            <tr>
                <td><c:out value="${department.getName()}"/></td>
                <td><c:out value="${department.getDescription()}"/></td>
                <td>
                    <c:forEach items="${
                    department.getEmployees().stream()
                    .map(emp -> emp.getLastName())
                    .toArray()}"
                    var="name">
                        <c:out value="${name}"/>
                    </c:forEach>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
