package app.builders;

import app.models.Position;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PositionBuilder {
    private static final String NAME_REGEX = "^[A-Z][a-z]{1,14}([- ][a-zA-Z]{1,15}){0,3}$";
    private static final int MIN_DESCRIPTION_SIZE = 10;
    private static final int MAX_DESCRIPTION_SIZE = 100;
    private static final double MIN_SALARY = 0;
    private static final double MAX_SALARY = 999999999;

    private String name = "";
    private String description = "";
    private double salary;

    public PositionBuilder() {}

    public PositionBuilder(String name) {
        this.name = name;
    }

    public PositionBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public PositionBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public PositionBuilder setSalary(double salary) {
        this.salary = salary;
        return this;
    }

    public Position build() {
        StringBuilder errorMessage = new StringBuilder("");

        Position position = new Position();
        Pattern namePattern = Pattern.compile(NAME_REGEX);

        Matcher nameMatcher = namePattern.matcher(name);

        if (!nameMatcher.matches())
            errorMessage.append("Enter correct deparment`s name");

        if (description.length() < MIN_DESCRIPTION_SIZE || description.length() > MAX_DESCRIPTION_SIZE) {
            errorMessage.append("Enter correct description");
        }

        if (salary < MIN_SALARY || salary > MAX_SALARY) {
            errorMessage.append("Enter correct employee list");
        }

//        if (errorMessage.toString().length() > 0) {
//            throw new IllegalArgumentException(errorMessage.toString());
//        }

        position.setName(name);
        position.setDescription(description);
        position.setSalary(salary);

        return position;
    }
}
