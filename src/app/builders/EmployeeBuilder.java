package app.builders;

import app.models.Employee;
import app.models.Position;

import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmployeeBuilder {
    private static final String NAME_REGEX = "^[A-Z][a-z]{1,14}([- ][a-zA-Z]{1,15}){0,3}[0-9]*$";
    private static final String PHONE_NUMBER_REGEX = "^(\\+?38)?(((099)|(098)|(097)|(096)|(095)|(068)|(067)|(066)|(065))\\d{7})$";
    private static final String EMAIL_REGEX = "^(?!.*@.*@.*$)(?!.*@.*\\-\\-.*\\..*$)(?!.*@.*\\-\\..*$)(?!.*@.*\\-$)(.*@.+(\\..{1,11})?)$";
    private static final String PASSPORT_REGEX = "^[A-Z]{2}[0-9]{8}$";
    private static final int MIN_AGE = 16;
    private static final int MAX_AGE = 60;

    private String firstName = "";
    private String lastName = "";
    private Position position = new Position();
    private String departmentName = "";
    private String passport = "";
    private LocalDate birthDay = LocalDate.now();
    private String phone = "";
    private String email = "";

    public EmployeeBuilder() { }

    public EmployeeBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public EmployeeBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public EmployeeBuilder setPosition(Position position) {
        this.position = position;
        return this;
    }

    public EmployeeBuilder setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
        return this;
    }

    public EmployeeBuilder setPassport(String passport) {
        this.passport = passport;
        return this;
    }

    public EmployeeBuilder setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
        return this;
    }

    public EmployeeBuilder setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public EmployeeBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public Employee build() {
        Employee employee = new Employee();
        StringBuilder errorMessage = new StringBuilder("");

        Pattern namesPattern = Pattern.compile(NAME_REGEX);
        Pattern emailPattern = Pattern.compile(EMAIL_REGEX);
        Pattern phoneNumberPattern = Pattern.compile(PHONE_NUMBER_REGEX);
        Pattern passportPattern = Pattern.compile(PASSPORT_REGEX);

        Matcher firstNameMatch = namesPattern.matcher(firstName);
        Matcher lastNameMatch = namesPattern.matcher(lastName);
        Matcher departmentNameMatch = namesPattern.matcher(departmentName);
        Matcher emailMatch = emailPattern.matcher(email);
        Matcher phoneNumberMatch = phoneNumberPattern.matcher(phone);
        Matcher passportMatch = passportPattern.matcher(passport);

        if(!firstNameMatch.matches())
        errorMessage.append("Enter correct firstName!\n");
        if(!lastNameMatch.matches())
            errorMessage.append("Enter correct lastName!\n");
        if(!phoneNumberMatch.matches())
            errorMessage.append("Enter correct phone number!\n");
        if(!emailMatch.matches())
            errorMessage.append("Enter correct e-mail!\n");
        if(!passportMatch.matches())
            errorMessage.append("Enter correct passport\n");
        if (!departmentNameMatch.matches())
            errorMessage.append("Enter correct department name\n");
        if(position.getSalary() < 0)
            errorMessage.append("Enter correct salary\n");
        if(LocalDate.now().compareTo(birthDay) > MAX_AGE || LocalDate.now().compareTo(birthDay) < MIN_AGE)
            errorMessage.append("Enter correct born year !\n");

//        if (errorMessage.toString().length() > 0) {
//            throw new IllegalArgumentException(errorMessage.toString());
//        }

        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setPassport(passport);
        employee.setPosition(position);
        employee.setDepartmentName(departmentName);
        employee.setEmail(email);
        employee.setPhone(phone);
        employee.setBirthDay(birthDay);

        return employee;
    }

    public static void main(String[] args) {
        Pattern namesPattern = Pattern.compile(NAME_REGEX);
        Matcher matcher = namesPattern.matcher("Rostyk1232323");
        System.out.println(matcher.matches());
    }

}
