package app.builders;

import app.models.Department;
import app.models.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DepartmentBuilder {
    private static final String NAME_REGEX = "^[A-Z][a-z]{1,14}([- ][a-zA-Z]{1,15}){0,3}$";
    private static final int MIN_DEPARTMENT_NUMBER = 2;
    private static final int MAX_DEPARTMENT_NUMBER = 20;
    private static final int MIN_DESCRIPTION_SIZE = 10;
    private static final int MAX_DESCRIPTION_SIZE = 100;

    private String name = "";
    private String description = "";
    private List<Employee> employees = new ArrayList<>();

    public DepartmentBuilder() { }

    public DepartmentBuilder(String name) {
        this.name = name;
    }

    public DepartmentBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public DepartmentBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public DepartmentBuilder setEmployees(List<Employee> employees) {
        this.employees = employees;
        return this;
    }

    public Department build() {
        StringBuilder errorMessage = new StringBuilder("");

        Department department = new Department();
        Pattern namePattern = Pattern.compile(NAME_REGEX);

        Matcher nameMatcher = namePattern.matcher(name);

        if (!nameMatcher.matches())
            errorMessage.append("Enter correct deparment`s name");
        if (employees.size() < MIN_DEPARTMENT_NUMBER || employees.size() > MAX_DEPARTMENT_NUMBER) {
            errorMessage.append("Enter correct employee list");
        }

        if (description.length() < MIN_DESCRIPTION_SIZE || description.length() > MAX_DESCRIPTION_SIZE) {
            errorMessage.append("Enter correct description");
        }

//        if(errorMessage.toString().length() > 0) {
//            throw new IllegalArgumentException(errorMessage.toString());
//        }

        department.setName(name);
        department.setDescription(description);
        department.setEmployees(employees);

        return department;
    }
}
