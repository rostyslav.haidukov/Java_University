package app.models;

import app.serialization.XmlLocalDateAdapter;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.LocalDate;

@XmlRootElement
@XmlType(propOrder = {"firstName", "lastName", "birthDay", "passport", "phone", "email", "departmentName", "position"})
public class Employee implements Serializable {
    private String firstName;
    private String lastName;
    private Position position;
    private String departmentName;
    private String passport;
    private LocalDate birthDay;
    private String phone;
    private String email;

    public Employee() {}

    public Employee(String firstName, String lastName, String passport, LocalDate birthDay) {
        this(firstName, lastName, passport, birthDay, null, null);
    }

    public Employee(String firstName, String lastName, String passport,
                    LocalDate birthDay, String phone, String email) {
        this(firstName, lastName, passport, birthDay, phone, email, null);
    }

    public Employee(String firstName, String lastName, String passport,
                    LocalDate birthDay, String phone, String email,
                    Position position) {
        this(firstName, lastName, passport, birthDay, phone, email, position, null);
    }

    public Employee(String firstName, String lastName, String passport,
                    LocalDate birthDay, String phone, String email,
                    Position position, String departmentName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.passport = passport;
        this.birthDay = birthDay;
        this.phone = phone;
        this.email = email;
        this.position = position;
        this.departmentName = departmentName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    @XmlJavaTypeAdapter(value = XmlLocalDateAdapter.class)
    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public String getBirthDayString() {
        return birthDay.toString();
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName=" + firstName +
                ", lastName=" + lastName +
                (position != null?
                ", positionName=" + position.getName() +
                ", salary=" + position.getSalary() +
                ", positionDescription=" + position.getDescription(): "") +
                ", departmentName=" + departmentName  +
                ", passport=" + passport +
                ", birthDay=" + birthDay +
                ", phone=" + phone +
                ", email=" + email +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;

        Employee employee = (Employee) o;

        if (firstName != null ? !firstName.equals(employee.firstName) : employee.firstName != null) return false;
        if (lastName != null ? !lastName.equals(employee.lastName) : employee.lastName != null) return false;
        if (position != null ? !position.equals(employee.position) : employee.position != null) return false;
        if (departmentName != null ? !departmentName.equals(employee.departmentName) : employee.departmentName != null)
            return false;
        if (passport != null ? !passport.equals(employee.passport) : employee.passport != null) return false;
        if (birthDay != null ? !birthDay.equals(employee.birthDay) : employee.birthDay != null) return false;
        if (phone != null ? !phone.equals(employee.phone) : employee.phone != null) return false;
        return email != null ? email.equals(employee.email) : employee.email == null;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (position != null ? position.hashCode() : 0);
        result = 31 * result + (departmentName != null ? departmentName.hashCode() : 0);
        result = 31 * result + (passport != null ? passport.hashCode() : 0);
        result = 31 * result + (birthDay != null ? birthDay.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }
}
