package app.serialization;

import app.models.Employee;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;

public class JsonSerializator<T extends Serializable> implements Serialization<T> {
    private Class<T> type;

    public JsonSerializator(Class<T> type) {
        this.type = type;
    }

    public Class<T> getType() {
        return type;
    }

    public void setType(Class<T> type) {
        this.type = type;
    }

    @Override
    public void toFile(T object, String path) throws IOException {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(LocalDate.class, new JsonLocalDateSerializator())
                .create();

        FileWriter outFile = null;

        try {
            outFile = new FileWriter(path);
            gson.toJson(object, outFile);
        } finally {
            if (outFile != null) {
                outFile.close();
            }
        }
    }

    @Override
    public T fromFile(String path) throws IOException {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(LocalDate.class, new JsonLocalDateDeserializator())
                .create();

        FileReader inFile = null;

        try {
            inFile = new FileReader(path);
            return gson.fromJson(inFile, type);
        } finally {
            if (inFile != null) {
                inFile.close();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Employee emp = new Employee("Rostyslav", "Haidukov", "TE23232", LocalDate.of(1999, 8, 7));
        Serialization<Employee> jsonSerialization = new JsonSerializator<>(Employee.class);
        jsonSerialization.toFile(emp, "./rostyk.json");
    }
}
