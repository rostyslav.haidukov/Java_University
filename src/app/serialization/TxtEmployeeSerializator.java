package app.serialization;

import app.builders.DepartmentBuilder;
import app.builders.EmployeeBuilder;
import app.builders.PositionBuilder;
import app.models.Employee;
import app.models.Position;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.*;
import java.time.LocalDate;
import java.util.Scanner;

public class TxtEmployeeSerializator implements Serialization<Employee> {

    @Override
    public void toFile(Employee emp, String path) throws Exception {
        FileWriter outFile = null;

        try {
            outFile = new FileWriter(path);
            outFile.write(emp.toString());
        }
        finally {
            if(outFile != null)
                outFile.close();
        }
    }

    @Override
    public Employee fromFile(String path) throws Exception {
        Employee employee = null;
        EmployeeBuilder employeeBuilder = null;
        FileReader inFile = null;
        Scanner input = null;
        String buff = null;
        String buffPrev = null;

        try {
            inFile = new FileReader(path);
            input = new Scanner(inFile);
            employeeBuilder = new EmployeeBuilder();
            input.useDelimiter("\n|(; ?)|:|( ?= ?)|, ");
            while(input.hasNext()) {
                buff = input.next().trim();
                switch(buff) {
                    case "firstName":
                        employeeBuilder.setFirstName(input.next().trim());
                        break;
                    case "lastName":
                        employeeBuilder.setLastName(input.next().trim());
                        break;
                    case "positionName":
                        String positionName = input.next().trim();
                        input.next();
                        Double salary = Double.parseDouble(input.next().trim());
                        input.next();
                        String positionDescription = input.next().trim();
                        Position position = new Position(positionName, salary, positionDescription);
                        employeeBuilder.setPosition(position);
                        break;
                    case "departmentName":
                        employeeBuilder.setDepartmentName(input.next().trim());
                        break;
                    case "passport":
                        employeeBuilder.setPassport(input.next().trim());
                        break;
                    case "birthDay":
                        employeeBuilder.setBirthDay(LocalDate.parse(input.next().trim()));
                        break;
                    case "phone":
                        employeeBuilder.setPhone(input.next().trim());
                        break;
                    case "email":
                        employeeBuilder.setEmail(input.next().trim());
                        break;
                }
            }
        }
        finally {
            if(inFile != null)
                inFile.close();
            if(input != null)
                input.close();
        }

        return employeeBuilder.build() ;
    }

    public static void main(String[] args) throws Exception {
        Employee employee = new Employee("Rostyslav", "Haidukov", "AB12345678", LocalDate.of(1999, 7, 8));
        employee.setPosition(new Position("fsdfdsf", 200, "fsf"));
        TxtEmployeeSerializator serializator = new TxtEmployeeSerializator();
        serializator.toFile(employee,"file.txt");
        System.out.println(serializator.fromFile("file.txt"));
    }
}
