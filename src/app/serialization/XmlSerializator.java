package app.serialization;

import app.models.Department;
import app.models.Employee;
import app.models.Position;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;

public class XmlSerializator<T extends Serializable> implements Serialization<T> {
    private Class<T> type;

    public XmlSerializator(Class<T> type) {
        this.type = type;
    }

    public Class<T> getType() {
        return type;
    }

    public void setType(Class<T> type) {
        this.type = type;
    }

    @Override
    public void toFile(T object, String path) throws IOException, JAXBException {
        JAXBContext context = null;
        FileWriter outFile = null;
        Marshaller marshaller = null;

        try {
            outFile = new FileWriter(path);
            context = JAXBContext.newInstance(type);
            marshaller = context.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(object, outFile);
        } finally {
            if (outFile != null) {
                outFile.close();
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public T fromFile(String path) throws IOException, JAXBException {
        JAXBContext context = null;
        Unmarshaller unmarshaller = null;
        FileReader inFile = null;

        try {
            inFile = new FileReader(path);
            context = JAXBContext.newInstance(type);
            unmarshaller = context.createUnmarshaller();
            return (T) unmarshaller.unmarshal(inFile);
        } finally {
            if (inFile != null) {
                inFile.close();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Position position = new Position("position", 1000, "desc");
        Serialization<Position> serialization = new XmlSerializator<>(Position.class);
        serialization.toFile(position, "position.xml");
        Employee emp = new Employee("Rostyslav", "Haidukov", "TE23232", LocalDate.of(1999, 8, 7));
        Department department = new Department("department name");
        emp.setDepartmentName(department.getName());
        department.addEmployee(emp);
        department.addEmployee(new Employee());
        Serialization<Employee> xmlSerialization = new XmlSerializator<>(Employee.class);
        xmlSerialization.toFile(emp, "./rostyk.xml");
        Serialization<Department> xmlSerialization2 = new XmlSerializator<>(Department.class);
        xmlSerialization2.toFile(department, "file.xml");
    }
}
