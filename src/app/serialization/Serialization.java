package app.serialization;

import java.io.IOException;
import java.io.Serializable;

public interface Serialization<T extends Serializable> {
    void toFile(T object, String path) throws Exception;
    T fromFile(String path) throws Exception;
}
