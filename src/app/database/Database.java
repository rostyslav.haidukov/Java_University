package app.database;

import app.builders.DepartmentBuilder;
import app.builders.EmployeeBuilder;
import app.builders.PositionBuilder;
import app.models.Department;
import app.models.Employee;
import app.models.Position;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Database {

    private static final String URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String USER = "postgres";
    private static final String PASSWORD = "postgres";
    private Connection connection;
    private static Database database;

    static {
        database = new Database();
    }

    private Database() {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager
                    .getConnection(URL, USER, PASSWORD);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public static Database getInstance() {
        return database;
    }

    public void createPositionTable() throws SQLException {
        String sqlRequest = "CREATE TABLE IF NOT EXISTS POSITION (" +
                "ID SERIAL NOT NULL PRIMARY KEY," +
                "NAME VARCHAR(255) NOT NULL CONSTRAINT _position_unique UNIQUE," +
                "DESCRIPTION VARCHAR(255)," +
                "SALARY DOUBLE PRECISION" +
                ")";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sqlRequest);
        }
    }

    public void createDepartmentTable() throws SQLException {
        String sqlRequest = "CREATE TABLE IF NOT EXISTS DEPARTMENT (" +
                "ID SERIAL NOT NULL PRIMARY KEY," +
                "NAME VARCHAR(255) NOT NULL CONSTRAINT name_unique UNIQUE," +
                "DESCRIPTION VARCHAR(255)" +
                ")";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sqlRequest);
        }
    }

    public void createEmployeeTable() throws SQLException {
        String sqlRequest = "CREATE TABLE IF NOT EXISTS EMPLOYEE (" +
                "ID SERIAL NOT NULL PRIMARY KEY," +
                "FIRST_NAME VARCHAR(255) NOT NULL," +
                "LAST_NAME VARCHAR(255) NOT NULL," +
                "POSITION_ID INT NOT NULL," +
                "DEPARTMENT_ID INT NOT NULL," +
                "PASSPORT VARCHAR(255) NOT NULL CONSTRAINT passport_unique UNIQUE," +
                "BIRTH_DATE DATE NOT NULL ," +
                "PHONE VARCHAR(255)," +
                "EMAIL VARCHAR(255)," +
                "FOREIGN KEY(POSITION_ID) REFERENCES POSITION(ID) ON DELETE CASCADE," +
                "FOREIGN KEY(DEPARTMENT_ID) REFERENCES DEPARTMENT(ID) ON DELETE CASCADE" +
                ")";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sqlRequest);
        }
    }

    public void addPosition(Position position) throws SQLException {
        String sqlRequest = "INSERT INTO POSITION (NAME, DESCRIPTION, SALARY) VALUES(?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sqlRequest)) {
            statement.setString(1, position.getName());
            statement.setString(2, position.getDescription());
            statement.setDouble(3, position.getSalary());
            statement.executeUpdate();

        }
    }

    private void addPosition(Position position, int id) throws SQLException {
        String sqlRequest = "INSERT INTO POSITION (ID, NAME, DESCRIPTION, SALARY) VALUES(?, ?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sqlRequest)) {
            statement.setInt(1, id);
            statement.setString(2, position.getName());
            statement.setString(3, position.getDescription());
            statement.setDouble(4, position.getSalary());
            statement.executeUpdate();

        }
    }

    public void addDepartment(Department department) throws SQLException {
        String sqlRequest = "INSERT INTO DEPARTMENT (NAME, DESCRIPTION) VALUES(?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sqlRequest)) {
            statement.setString(1, department.getName());
            statement.setString(2, department.getDescription());
            statement.executeUpdate();

        }
    }

    private void addDepartment(Department department, int id) throws SQLException {
        String sqlRequest = "INSERT INTO DEPARTMENT (ID, NAME, DESCRIPTION) VALUES(?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sqlRequest)) {
            statement.setInt(1, id);
            statement.setString(2, department.getName());
            statement.setString(3, department.getDescription());
            statement.executeUpdate();

        }
    }

    public void addEmployee(Employee employee) throws SQLException {
        String sqlRequest = "INSERT INTO EMPLOYEE (" +
                "FIRST_NAME, LAST_NAME, POSITION_ID, DEPARTMENT_ID, PASSPORT, BIRTH_DATE, PHONE, EMAIL" +
                ")" +
                " VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sqlRequest)) {
            int departmentId = getDepartmentId(employee.getDepartmentName());
            int positionId = getPositionId(employee.getPosition().getName());
            if (departmentId == -1) throw new IllegalArgumentException("Incorrect department");
            if (positionId == -1) throw new IllegalArgumentException("Incorrect position");
            statement.setString(1, employee.getFirstName());
            statement.setString(2, employee.getLastName());
            statement.setInt(3, positionId);
            statement.setInt(4, departmentId);
            statement.setString(5, employee.getPassport());
            statement.setDate(6, Date.valueOf(employee.getBirthDayString()));
            statement.setString(7, employee.getPhone());
            statement.setString(8, employee.getEmail());
            statement.executeUpdate();
        }
    }

    private void addEmployee(Employee employee, int id) throws SQLException {
        String sqlRequest = "INSERT INTO EMPLOYEE (" +
                "ID, FIRST_NAME, LAST_NAME, POSITION_ID, DEPARTMENT_ID, PASSPORT, BIRTH_DATE, PHONE, EMAIL" +
                ")" +
                " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sqlRequest)) {
            int departmentId = getDepartmentId(employee.getDepartmentName());
            int positionId = getPositionId(employee.getPosition().getName());
            if (departmentId == -1) throw new IllegalArgumentException("Incorrect department");
            if (positionId == -1) throw new IllegalArgumentException("Incorrect position");
            statement.setInt(1, id);
            statement.setString(2, employee.getFirstName());
            statement.setString(3, employee.getLastName());
            statement.setInt(4, positionId);
            statement.setInt(5, departmentId);
            statement.setString(6, employee.getPassport());
            statement.setDate(7, Date.valueOf(employee.getBirthDayString()));
            statement.setString(8, employee.getPhone());
            statement.setString(9, employee.getEmail());
            statement.executeUpdate();
        }
    }

    public List<Employee> getAllEmployees() throws SQLException {
        String sqlRequest = "SELECT EMPLOYEE.FIRST_NAME, EMPLOYEE.LAST_NAME, EMPLOYEE.PASSPORT," +
                "EMPLOYEE.BIRTH_DATE, EMPLOYEE.PHONE, EMPLOYEE.EMAIL," +
                "POSITION.NAME, POSITION.DESCRIPTION, POSITION.SALARY," +
                "DEPARTMENT.NAME " +
                "FROM EMPLOYEE " +
                "INNER JOIN POSITION ON EMPLOYEE.POSITION_ID = POSITION.ID " +
                "INNER JOIN DEPARTMENT ON EMPLOYEE.DEPARTMENT_ID = DEPARTMENT.ID";
        EmployeeBuilder builder = new EmployeeBuilder();
        try (Statement statement = connection.createStatement()) {
            List<Employee> list = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery(sqlRequest);
            while (resultSet.next()) {
                PositionBuilder positionBuilder = new PositionBuilder();
                Position position = positionBuilder
                        .setName(resultSet.getString(7))
                        .setDescription(resultSet.getString(8))
                        .setSalary(resultSet.getDouble(9))
                        .build();
                String departmentDame = resultSet.getString(10);
                Employee employee = builder
                        .setFirstName(resultSet.getString(1))
                        .setLastName(resultSet.getString(2))
                        .setPassport(resultSet.getString(3))
                        .setBirthDay(LocalDate.parse(resultSet.getString(4)))
                        .setPhone(resultSet.getString(5))
                        .setEmail(resultSet.getString(6))
                        .setPosition(position)
                        .setDepartmentName(departmentDame)
                        .build();
                list.add(employee);
            }
            return list;
        }
    }

    public List<Position> getAllPositions() throws SQLException {
        String sqlRequest = "SELECT name, description, salary FROM POSITION";
        List<Position> list = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlRequest);
            while (resultSet.next()) {
                PositionBuilder positionBuilder = new PositionBuilder();
                Position position = positionBuilder
                        .setName(resultSet.getString(1))
                        .setDescription(resultSet.getString(2))
                        .setSalary(resultSet.getDouble(3))
                        .build();
                list.add(position);
            }
            return list;
        }
    }

    public List<Department> getAllDepartments() throws SQLException {
        String sqlRequest = "SELECT name, description FROM DEPARTMENT";
        List<Department> list = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlRequest);
            while (resultSet.next()) {
                DepartmentBuilder departmentBuilder = new DepartmentBuilder();
                String name = resultSet.getString(1);
                String description = resultSet.getString(2);
                List<Employee> employees = getAllEmployeesByDepartmentName(name);
                Department department = departmentBuilder
                        .setName(name)
                        .setDescription(description)
                        .setEmployees(employees)
                        .build();
                list.add(department);
            }
            return list;
        }
    }

    public List<Employee> getAllEmployeesByDepartmentName(String name) throws SQLException {
        String sqlRequest = "SELECT EMPLOYEE.FIRST_NAME, EMPLOYEE.LAST_NAME, EMPLOYEE.PASSPORT," +
                "EMPLOYEE.BIRTH_DATE, EMPLOYEE.PHONE, EMPLOYEE.EMAIL," +
                "POSITION.NAME, POSITION.DESCRIPTION, POSITION.SALARY " +
                "FROM DEPARTMENT " +
                "INNER JOIN EMPLOYEE ON DEPARTMENT.ID = EMPLOYEE.DEPARTMENT_ID " +
                "INNER JOIN POSITION ON POSITION.ID = EMPLOYEE.POSITION_ID " +
                "WHERE DEPARTMENT.NAME = ?";
        EmployeeBuilder builder = new EmployeeBuilder();
        try (PreparedStatement statement = connection.prepareStatement(sqlRequest)) {
            List<Employee> list = new ArrayList<>();
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                PositionBuilder positionBuilder = new PositionBuilder();
                Position position = positionBuilder
                        .setName(resultSet.getString(7))
                        .setDescription(resultSet.getString(8))
                        .setSalary(resultSet.getDouble(9))
                        .build();
                Employee employee = builder
                        .setFirstName(resultSet.getString(1))
                        .setLastName(resultSet.getString(2))
                        .setPassport(resultSet.getString(3))
                        .setBirthDay(LocalDate.parse(resultSet.getString(4)))
                        .setPhone(resultSet.getString(5))
                        .setEmail(resultSet.getString(6))
                        .setPosition(position)
                        .setDepartmentName(name)
                        .build();
                list.add(employee);
            }
            return list;
        }
    }

    public List<Employee> getAllEmployeesByPositionName(String name) throws SQLException {
        String sqlRequest = "SELECT EMPLOYEE.FIRST_NAME, EMPLOYEE.LAST_NAME, EMPLOYEE.PASSPORT," +
                "EMPLOYEE.BIRTH_DATE, EMPLOYEE.PHONE, EMPLOYEE.EMAIL," +
                "POSITION.NAME, POSITION.DESCRIPTION, POSITION.SALARY," +
                "DEPARTMENT.NAME " +
                "FROM DEPARTMENT " +
                "INNER JOIN EMPLOYEE ON DEPARTMENT.ID = EMPLOYEE.DEPARTMENT_ID " +
                "INNER JOIN POSITION ON POSITION.ID = EMPLOYEE.POSITION_ID " +
                "WHERE POSITION .NAME = ?";
        EmployeeBuilder builder = new EmployeeBuilder();
        try (PreparedStatement statement = connection.prepareStatement(sqlRequest)) {
            List<Employee> list = new ArrayList<>();
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                PositionBuilder positionBuilder = new PositionBuilder();
                Position position = positionBuilder
                        .setName(name)
                        .setDescription(resultSet.getString(8))
                        .setSalary(resultSet.getDouble(9))
                        .build();
                Employee employee = builder
                        .setFirstName(resultSet.getString(1))
                        .setLastName(resultSet.getString(2))
                        .setPassport(resultSet.getString(3))
                        .setBirthDay(LocalDate.parse(resultSet.getString(4)))
                        .setPhone(resultSet.getString(5))
                        .setEmail(resultSet.getString(6))
                        .setPosition(position)
                        .setDepartmentName(resultSet.getString(10))
                        .build();
                list.add(employee);
            }
            return list;
        }
    }

    public int getDepartmentId(String name)  {
        String sqlRequest = "SELECT * FROM DEPARTMENT WHERE NAME=?;";
        return getIdByQuery(name, sqlRequest);
    }


    public int getPositionId(String name) {
        String sqlRequest = "SELECT * FROM POSITION WHERE NAME=?;";
        return getIdByQuery(name, sqlRequest);
    }

    public int getEmployeeId(String passport) {
        String sqlRequest = "SELECT * FROM EMPLOYEE WHERE PASSPORT=?;";
        return getIdByQuery(passport, sqlRequest);
    }

    private int getIdByQuery(String name, String sqlRequest) {
        try (PreparedStatement statement = connection.prepareStatement(sqlRequest)) {
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt("id");
        } catch (SQLException exception) {
            return -1;
        }
    }

    public Employee getEmployeeByPassport(String passport) throws SQLException {
        String sqlRequest = "SELECT EMPLOYEE.FIRST_NAME, EMPLOYEE.LAST_NAME, EMPLOYEE.PASSPORT," +
                "EMPLOYEE.BIRTH_DATE, EMPLOYEE.PHONE, EMPLOYEE.EMAIL," +
                "POSITION.NAME, POSITION.DESCRIPTION, POSITION.SALARY," +
                "DEPARTMENT.NAME " +
                "FROM DEPARTMENT " +
                "INNER JOIN EMPLOYEE ON DEPARTMENT.ID = EMPLOYEE.DEPARTMENT_ID " +
                "INNER JOIN POSITION ON POSITION.ID = EMPLOYEE.POSITION_ID " +
                "WHERE EMPLOYEE.PASSPORT = ?";
        EmployeeBuilder builder = new EmployeeBuilder();
        try (PreparedStatement statement = connection.prepareStatement(sqlRequest)) {
            statement.setString(1, passport);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            PositionBuilder positionBuilder = new PositionBuilder();
            Position position = positionBuilder
                    .setName(resultSet.getString(7))
                    .setDescription(resultSet.getString(8))
                    .setSalary(resultSet.getDouble(9))
                    .build();
            Employee employee = builder
                    .setFirstName(resultSet.getString(1))
                    .setLastName(resultSet.getString(2))
                    .setPassport(resultSet.getString(3))
                    .setBirthDay(LocalDate.parse(resultSet.getString(4)))
                    .setPhone(resultSet.getString(5))
                    .setEmail(resultSet.getString(6))
                    .setPosition(position)
                    .setDepartmentName(resultSet.getString(10))
                    .build();
            return employee;
        }
    }

    public Position getPositionByName(String name) throws SQLException {
        String sqlRequest = "SELECT name, description, salary " +
                "FROM POSITION " +
                "WHERE name = ?";
        try (PreparedStatement statement = connection.prepareStatement(sqlRequest)) {
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            PositionBuilder positionBuilder = new PositionBuilder();
            return positionBuilder
                    .setName(resultSet.getString(1))
                    .setDescription(resultSet.getString(2))
                    .setSalary(resultSet.getDouble(3))
                    .build();
        }
    }

    public Department getDepartmentByName(String name) throws SQLException {
        String sqlRequest = "SELECT name, description FROM DEPARTMENT";
        try (PreparedStatement statement = connection.prepareStatement(sqlRequest)) {
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            DepartmentBuilder departmentBuilder = new DepartmentBuilder();
            String description = resultSet.getString(2);
            List<Employee> employees = getAllEmployeesByDepartmentName(name);
            return departmentBuilder
                    .setName(name)
                    .setDescription(resultSet.getString(2))
                    .setEmployees(employees)
                    .build();
        }
    }

    public void deletePosition(String name) throws SQLException {
        String sqlRequest = "DELETE FROM DEPARTMENT WHERE NAME = ?";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sqlRequest);
        }
    }

    public void deleteEmployee(String name) throws SQLException {
        String sqlRequest = "DELETE FROM EMPLOYEE WHERE NAME = ?";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sqlRequest);
        }
    }

    public void deleteDepartment(String name) throws SQLException {
        String sqlRequest = "DELETE FROM EMPLOYEE WHERE NAME = ?";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sqlRequest);
        }
    }

    public void updatePosition(String name, Position position) throws SQLException {
        connection.setAutoCommit(Boolean.FALSE);
        int id = getPositionId(name);
        try {
            deletePosition(name);
            connection.commit();
            addPosition(position, id);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.setAutoCommit(Boolean.TRUE);
        }
    }

    public void updateDepartment(String name, Department department) throws SQLException {
        connection.setAutoCommit(Boolean.FALSE);
        int id = getDepartmentId(name);
        try {
            deleteDepartment(name);
            connection.commit();
            addDepartment(department, id);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.setAutoCommit(Boolean.TRUE);
        }
    }

    public void updateEmployee(String passport, Employee employee) throws SQLException {
        connection.setAutoCommit(Boolean.FALSE);
        int id = getEmployeeId(passport);
        try {
            deleteEmployee(passport);
            connection.commit();
            addEmployee(employee, id);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.setAutoCommit(Boolean.TRUE);
        }
    }

    public static void main(String[] args) throws SQLException {
        Database db = Database.getInstance();
        List<Position> list = db.getAllPositions();
        System.out.println(list);

    }
}
