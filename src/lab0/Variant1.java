package lab0;

import org.testng.asserts.Assertion;

public class Variant1 {

    private int field1;

    public enum DAY_OF_WEEK {
        MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
    }

    /**
     * @param k is square side
     * @return perimeter
     */
    public int inputOutputTask(int k) {
        assert k > 0 : "Argument should be more than zero";
        return 4 * k;
    }

    /**
     * @param k is distance in cm
     * @return distance in m
     */

    public int integerNumbersTask(int k) {
        assert k > 0 : "Argument should be more than zero";
        return k % 100;
    }

    /**
     * @param number
     * @return true, if number is positive
     */
    public boolean booleanTask(int number) {
        return number > 0;
    }


    /**
     * @param parameter is integer number
     * @return if it positive increment
     */
    public int ifTask(int parameter) {
        if (booleanTask(parameter)) {
            parameter++;
        }
        return parameter;
    }


    /**
     * @param number
     * @return day of week day represented number
     */
    public DAY_OF_WEEK switchTask(int number) {
        assert number > 0 && number <= 7 : "Argument should be more than zero and less than 8";
        DAY_OF_WEEK dayOfWeek = null;
        switch (number) {
            case 1:
                dayOfWeek = DAY_OF_WEEK.MONDAY;
                break;
            case 2:
                dayOfWeek = DAY_OF_WEEK.TUESDAY;
                break;
            case 3:
                dayOfWeek = DAY_OF_WEEK.WEDNESDAY;
                break;
            case 4:
                dayOfWeek = DAY_OF_WEEK.THURSDAY;
                break;
            case 5:
                dayOfWeek = DAY_OF_WEEK.FRIDAY;
                break;
            case 6:
                dayOfWeek = DAY_OF_WEEK.SATURDAY;
                break;
            case 7:
                dayOfWeek = DAY_OF_WEEK.SUNDAY;
                break;
        }
        return dayOfWeek;
    }


    /**
     * @param n is integer number
     * @return approximated value of exp(1)
     */
    public double forTask(int n) {
        assert n > 0 : "Argument should be more than zero";
        double exp = 1;
        double sumEl = 1;
        for (int i = 1; i <= n; i++) {
            sumEl /= i;
            exp += sumEl;
        }
        return exp;
    }

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public int whileTask(int a, int b) {
        assert (a > 0 && b > 0) : "Argument should be more than zero";
        assert (a >= b) : "The first argument should be more or equal than second";
        int c = 0;
        while (a >= b) {
            a -= b;
        }
        return a - c;
    }

    public double arrayTask(double[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] < array[array.length - 1]) return array[i];
        }
        return 0;
    }

    /**
     * @param array
     * @param k1
     * @param k2
     * @return transformed array where row with indexes k1 and k2 was changed
     */
    public int[][] twoDimensionArrayTask(int[][] array, int k1, int k2) {
        assert k1 <= array.length && k2 <= array.length : "Arguments should be less or equal to array length";
        k1 = k1 - 1;
        k2 = k2 - 1;
        int[][] arrayCp = new int[array.length][];
        for(int i = 0; i < array.length; i++) {
            arrayCp[i] = new int[array[i].length];
            for (int j = 0; j < array[i].length; j++) {
                arrayCp[i][j] = array[i][j];
            }
        }

        int[] row = arrayCp[k1];
        arrayCp[k1] = arrayCp[k2];
        arrayCp[k2] = row;
        return arrayCp;
    }

    public int methInteger(Integer arg) {
        arg = 24;
        return arg;
    }

    public Variant1 methField(Variant1 arg) {
        arg.field1 = 24;
        return new Variant1();
    }


    public static void main(String... strings) {
        System.out.println("Start of zero lab0");
        System.out.println("Done!!!");
        System.out.println(new Variant1().whileTask(12, 10));
        Variant1 k = new Variant1();
        k.field1 = 356;
        System.out.println(new Variant1().inputOutputTask(-2));
        System.out.println("k= " + k.field1);
    }

}
